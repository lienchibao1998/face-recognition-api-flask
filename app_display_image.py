from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from flask import Flask, request, render_template, send_from_directory, jsonify


import tensorflow as tf
from scipy import misc
import cv2
import numpy as np
import facenet
import detect_face
import os
import pickle
import json
import time


app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

@app.route("/")
def index():
    return render_template("upload1.html")

@app.route("/upload", methods=["POST"])
def upload():

    print("POST")
    target = os.path.join(APP_ROOT, 'images/')

    print(target)
    if not os.path.isdir(target):
            os.mkdir(target)
    else:
        print("Couldn't create upload directory: {}".format(target))
    print(request.files.getlist("file"))
    for upload in request.files.getlist("file"):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = upload.filename
        destination = "/".join([target, filename])
        print ("Accept incoming file:", filename)
        print ("Save it to:", destination)
        print("Destination: ....", destination)
        upload.save(destination)
        img = os.getcwd() + "/images/" + filename
        # print("Image: ", img)

        print('Creating networks and loading parameters')
        with tf.Graph().as_default():
            gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
            sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
            with sess.as_default():
                pnet, rnet, onet = detect_face.create_mtcnn(sess, os.getcwd() + '/align')

                minsize = 40  # minimum size of face
                threshold = [0.6, 0.8, 0.92]  # three steps's threshold
                factor = 0.709  # scale factor
                margin = 44
                frame_interval = 3
                batch_size = 100
                image_size = 182
                input_image_size = 160

                currTime = time.time()
                with open('data.txt') as json_file:
                    HumanNames = json.load(json_file)

                presentTime = time.time()
                print("Loading time ", presentTime - currTime)

                print('Loading feature extraction model')


                modeldir = os.getcwd() + '/models/facenet/20170512-110547.pb'
                facenet.load_model(modeldir)


                images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
                embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
                phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
                embedding_size = embeddings.get_shape()[1]

                classifier_filename = os.getcwd() + '/models/classifier/classifier.pkl'
                classifier_filename_exp = os.path.expanduser(classifier_filename)
                with open(classifier_filename_exp, 'rb') as infile:
                    (model, class_names) = pickle.load(infile)
                    print('load classifier file-> %s' % classifier_filename_exp)



                print('Start Recognition!')
                prevTime = 0
                c = 0

                frame = cv2.imread(img, 0)
                image = cv2.imread(img)
                # frame = cv2.resize(frame, (0, 0), fx=0.4, fy=0.4)  # resize frame (optional)

                curTime = time.time() + 1  # calc fps
                timeF = frame_interval

                if (c % timeF == 0):
                    find_results = []

                    if frame.ndim == 2:
                        frame = facenet.to_rgb(frame)

                    frame = frame[:, :, 0:3]
                    bounding_boxes, _ = detect_face.detect_face(frame, minsize, pnet, rnet, onet, threshold, factor)
                    nrof_faces = bounding_boxes.shape[0]
                    print('Face Detected: %d' % nrof_faces)

                    if nrof_faces > 0:
                        det = bounding_boxes[:, 0:4]
                        img_size = np.asarray(frame.shape)[0:2]

                        cropped = []
                        scaled = []
                        scaled_reshape = []
                        bb = np.zeros((nrof_faces, 4), dtype=np.int32)

                        # j = 0
                        for i in range(nrof_faces):
                            emb_array = np.zeros((1, embedding_size))

                            bb[i][0] = det[i][0]
                            bb[i][1] = det[i][1]
                            bb[i][2] = det[i][2]
                            bb[i][3] = det[i][3]

                            # inner exception
                            if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= len(frame[0]) or bb[i][3] >= len(frame):
                                print('face is too close')
                                continue

                            cropped.append(frame[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])
                            cropped[i] = facenet.flip(cropped[i], False)
                            scaled.append(misc.imresize(cropped[i], (image_size, image_size), interp='bilinear'))
                            scaled[i] = cv2.resize(scaled[i], (input_image_size, input_image_size),
                                                   interpolation=cv2.INTER_CUBIC)
                            scaled[i] = facenet.prewhiten(scaled[i])
                            scaled_reshape.append(scaled[i].reshape(-1, input_image_size, input_image_size, 3))
                            feed_dict = {images_placeholder: scaled_reshape[i], phase_train_placeholder: False}
                            emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)
                            predictions = model.predict_proba(emb_array)
                            print("Prediction: ", predictions)
                            # print(predictions)
                            best_class_indices = np.argmax(predictions, axis=1)
                            # print(best_class_indices)
                            best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]

                            print("Best class probability: ", best_class_probabilities)
                            cv2.rectangle(image, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)  # boxing face

                            # plot result idx under box
                            text_x = bb[i][0]
                            text_y = bb[i][3] + 20
                            print('Result Indices: ', best_class_indices[0])
                            # print(HumanNames)

                            for H_i in HumanNames:
                                # print(H_i)
                                if HumanNames[best_class_indices[0]] == H_i:
                                    result_names = HumanNames[best_class_indices[0]]
                                    if best_class_probabilities >= 0.8:
                                        print(result_names)
                                        name  = result_names
                                        id = best_class_indices[0].astype(np.str)
                                    else:
                                        name = "Unknown"
                                        id = None
                    else:
                        print('Unable to align')
                filename1 = filename.split('.')[-2]
                filename_predicted = "_".join([filename1, 'predicted.jpg'])
                print("Filename predicted:  ", filename_predicted)

                cv2.imwrite(os.getcwd() + '/images/' + filename_predicted, image)

    # return render_template("image.html", image_name=filename_predicted)
    finalTime = time.time()
    sec = finalTime - currTime
    print("Time: ", sec)
    return jsonify(name=name)

@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory("images", filename)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5556, debug=True)
